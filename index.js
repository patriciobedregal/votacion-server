const express = require("express");
const mysql = require("mysql");
const generatePassword = require("password-generator");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cluster = require("cluster");
const http = require("http");
const multer = require("multer");
const fs = require("fs");
const mongo = require("mongodb").MongoClient;
const assert = require("assert");
const app = express();
const jwt = require("jsonwebtoken");
let numCPUs = require("os").cpus().length;
let exec = require("child_process").exec,
  child;
let port = process.env.PORT || 3000;
let url =
  "mongodb://heroku_3rzr39lq:jvnn03s8oqrckiuhrnln1fjkp3@ds125841.mlab.com:25841/heroku_3rzr39lq";

/*if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  app.listen(port, function() {
    console.log("listening in http://localhost:" + port);
  });

  console.log(`Worker ${process.pid} started`);
}
*/

let db;
// Create connection
let db_config = {
  host: "us-cdbr-iron-east-04.cleardb.net",
  user: "b936ae846b9c51",
  password: "dc4d5ee1",
  database: "heroku_d4ad6fac5ac39cd"
};

let store = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./logos");
  },
  filename: function(req, file, cb) {
    console.log(file);
    let type = file.mimetype;
    type = type.split("/");
    type = type[1];
    cb(null, Date.now() + "." + file.originalname + "." + type);
  }
});

let upload = multer({ storage: store }).single("file");

function handleDisconnect() {
  db = mysql.createConnection(db_config); // Recreate the connection, since
  // the old one cannot be reused.

  db.connect(function(err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log("error when connecting to db:", err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }
    console.log("MySql Connected...");
    // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  db.on("error", function(err) {
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server letiable configures this)
    }
  });
}
app.listen(port, function() {
  console.log("listening in http://localhost:" + port);
});
handleDisconnect();

app.use(morgan("dev"));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(function(req, res, next) {
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.post("/login", (req, res) => {
  console.log(req.body);
  if (req.body.codigo === undefined || req.body.password === undefined) {
    res.send({
      estado: false
    });
  } else {
    let log = logeo(req, res);
  }
});

function logeo(object, options) {
  let codigo = object.body.codigo;
  let password = object.body.password;
  console.log(object.body);

  let sql =
    "SELECT * FROM administrador WHERE usuario= '" +
    codigo +
    "' and contrasena ='" +
    password +
    "'";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    if (result[0] === undefined) {
      logeoMesa(object, options);
    } else {
      options.send({
        estado: true,
        type: "admin",
        estado: result[0].publicado,
        generado: result[0].generado
      });
    }
    return result;
  });
  return query;
}
function logeoMesa(object, options) {
  let codigo = object.body.codigo;
  let password = object.body.password;
  let sql =
    "SELECT * FROM mesa WHERE codigo= '" +
    codigo +
    "' and contrasena ='" +
    password +
    "'";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log("resultado" + result);
    if (result[0] === undefined) {
      options.send({
        estado: false
      });
    } else {
      options.send({
        estado: true,
        type: "mesa",
        id: result[0].id
      });
    }
    return result;
  });
  return query;
}

app.get("/facultades", (req, res) => {
  let sql = "SELECT * FROM facultad";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});

app.get("/mesas", (req, res) => {
  let sql = "SELECT * FROM mesa";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});

app.get("/codigos", (req, res) => {
  let sql = "SELECT * FROM codigo";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});

app.get("/partidos", (req, res) => {
  let sql = "SELECT * FROM partido";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});
app.get("/categorias", (req, res) => {
  let sql = "SELECT * FROM categoria";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});
app.get("/votos", (req, res) => {
  let sql = "SELECT * FROM voto";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});
app.get("/votosMesa", (req, res) => {
  let data;
  mongo.connect(
    url,
    function(err, db) {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      var dbo = db.db("heroku_3rzr39lq");
      dbo
        .collection("votos")
        .find({})
        .toArray(function(err, result) {
          if (err) {
            return res.status(500).json({
              err: err,
              ok: false
            });
          }
          console.log(result);
          data = JSON.stringify(result);
          data = data.replace("[", "");
          data = data.replace("]", "");
          data = data.replace(/},/g, "}\n");
          console.log(data);
          var resultados = toFileM(data, res);
          console.log(resultados);

          db.close();
        });
    }
  );
});
app.get("/votosGenerales", (req, res) => {
  let data;
  mongo.connect(
    url,
    function(err, db) {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      var dbo = db.db("heroku_3rzr39lq");
      dbo
        .collection("votos")
        .find({})
        .toArray(function(err, result) {
          if (err) {
            return res.status(500).json({
              err: err,
              ok: false
            });
          }
          console.log(result);
          data = JSON.stringify(result);
          data = data.replace("[", "");
          data = data.replace("]", "");
          data = data.replace(/},/g, "}\n");
          console.log(data);
          var resultados = toFile(data, res);
          console.log(resultados);

          db.close();
        });
    }
  );
});

function toFile(data, res) {
  fs.writeFile("file", data, function(err) {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log("Saved!");
    var conteo = exc(res);
    console.log();
    console.log(conteo);
  });
}
function toFileM(data, res) {
  fs.writeFile("file", data, function(err) {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log("Saved!");
    var conteo = excMesa(res);
    console.log();
    console.log(conteo);
  });
}
function excMesa(res) {
  child = exec(
    "cat file | node mapper2.js | sort -k1,1 | node reducer.js",
    // Pasamos los parámetros error, stdout la salida
    // que mostrara el comando
    function(error, stdout, stderr) {
      // Imprimimos en pantalla con console.log
      console.log(stdout);
      // controlamos el error
      if (error !== null) {
        console.log("exec error: " + error);
      } else {
        console.log(stdout);

        res.send({ res: stdout });
      }
    }
  );
}
function exc(res) {
  child = exec(
    "cat file | node mapper.js | sort -k1,1 | node reducer.js",
    // Pasamos los parámetros error, stdout la salida
    // que mostrara el comando
    function(error, stdout, stderr) {
      // Imprimimos en pantalla con console.log
      console.log(stdout);
      // controlamos el error
      if (error !== null) {
        console.log("exec error: " + error);
      } else {
        console.log(stdout);

        res.send({ res: stdout });
      }
    }
  );
}

//generar mesas de acuerdo a un numero
//asignar un alumno a una mesa de acuerdo a su facultad
app.post("/generarmesas", (req, res) => {
  let votantesMesa = req.body.votantesMesa;
  let sqlFacultades =
    "select facultad_id, count(facultad_id) as numAl from alumno group by facultad_id";
  let qFacultades = db.query(sqlFacultades, (err, rFacultades) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    let contadormesa = 0;
    rFacultades.forEach(x => {
      let numMesas = x.numAl / votantesMesa;
      numMesas = Math.ceil(numMesas);
      while (numMesas != 0) {
        contadormesa++;
        let reqM = {
          body: {
            numVotantes: votantesMesa,
            facultadId: x.facultad_id,
            codigo: "MESA" + contadormesa
          }
        };
        agregarmesa(reqM, null);
        numMesas--;
      }
    });
  });
  let sql2 = "UPDATE administrador SET generado = 1 where 1 = 1";
  db.query(sql2, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send({ estado: true });
  });
});

app.post("/votospormesa", (req, res) => {
  let mesaId = req.body.mesaId;
  console.log(mesaId);
  let sql = "SELECT * FROM voto WHERE mesa_id = '" + mesaId + "'";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});
app.post("/codigospormesa", (req, res) => {
  let mesaId = req.body.mesaId;
  console.log(mesaId);
  let sql = "SELECT * FROM codigo WHERE mesa_id = '" + mesaId + "'";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send(result);
  });
});

app.post("/verificarVotacion", (req, res) => {
  let mesaId = req.body.mesaId;
  console.log(mesaId);
  let sql = "SELECT estado FROM mesa WHERE mesa_id = '" + mesaId + "'";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    if (result[0] == undefined) {
      res.send({ estado: false });
    } else {
      if (result[0].estado == "1" || result[0].estado == "0") {
        res.send({ estado: false });
      } else {
        res.send({ estado: true });
      }
    }
  });
});

app.post("/mesa", (req, res) => {
  agregarmesa(req, res);
  res.send({ estado: "Mesa agregada..." });
});

function agregarmesa(req, res) {
  console.log(req.body);
  let contra = generatePassword(5, false);
  let numVotantes = req.body.numVotantes;
  let facultadId = req.body.facultadId;
  let codigo = req.body.codigo;
  let post = { title: "Post mesa", body: "This is post number one" };
  let sql =
    "INSERT INTO mesa ( facultad_id, contrasena, numero_votantes_total, estado, iniciado, terminado, codigo, entregado) VALUES('" +
    facultadId +
    "', '" +
    contra +
    "', '" +
    numVotantes +
    "','0','','','" +
    codigo +
    "', '0')";
  let query = db.query(sql, post, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log(result);
    let mesaId = result.insertId;
    let numero = numVotantes;

    while (numero != 0) {
      let prefijo = numero + "-";
      let codigo = generatePassword(10, false, /\d/, prefijo);
      let post = { title: "Post codigo", body: "This is post number one" };
      let sql =
        "INSERT INTO codigo ( mesa_id, codigo, estado) VALUES('" +
        mesaId +
        "','" +
        codigo +
        "','true')";
      let query = db.query(sql, post, (err, result) => {
        if (err) {
          return res.status(500).json({
            err: err,
            ok: false
          });
        }
        console.log(true);
      });
      numero--;
    }
    let sqlAlumnos =
      "update alumno set mesa_id =" +
      mesaId +
      " where facultad_id = " +
      facultadId +
      " and mesa_id is NULL limit " +
      numVotantes;
    let qAlumnos = db.query(sqlAlumnos, (err, rAlumnos) => {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      console.log("okay alumnos relacionados");
    });
    return result.insertId;
  });
  return query;
}
app.post("/partido", (req, res) => {
  let nombre = req.body.nombre;
  let post = { title: "Post partido", body: "This is post number one" };
  let sql = "INSERT INTO partido ( nombre ) VALUES('" + nombre + "')";

  let query = db.query(sql, post, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    let partidoId = result.insertId;
    console.log(partidoId);

    let sql2 = "SELECT * FROM categoria";
    db.query(sql2, (err, result2) => {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      result2.forEach(categoria => {
        let categoriaId = categoria.id;
        let post = {
          title: "Post categoria_x_partido",
          body: "This is post number one"
        };
        let sql3 =
          "INSERT INTO categoria_x_partido ( categoria_id, partido_id) VALUES('" +
          categoriaId +
          "', '" +
          partidoId +
          "')";
        db.query(sql3, post, (err, result3) => {
          if (err) {
            return res.status(500).json({
              err: err,
              ok: false
            });
          }
          console.log(true);
        });
      });
      return result;
    });
    res.send({ estado: "ok" });
  });
});

app.post("/upload", function(req, res, next) {
  upload(req, res, function(err) {
    if (err) {
      return res.status(501).json({ error: err });
    }
    let name = req.file.filename.split(".");
    name = name[1];
    console.log(req.file.filename);
    let sql =
      "UPDATE partido SET logo = '" +
      req.file.filename +
      "' WHERE nombre = '" +
      name +
      "'";
    db.query(sql, (err, result) => {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      res.send({ estado: "ok" });
    });
  });
});

app.get("/download/:image", function(req, res, next) {
  let type = req.params.image.split(".")[2];
  filepath = "./logos/" + req.params.image;
  let img = fs.readFileSync(filepath);
  res.writeHead(200, { "Content-Type": "image/" + type });
  res.end(img, "binary");
});

app.post("/categoria", (req, res) => {
  let nombre = req.body.nombre;
  let post = { title: "Post categoria", body: "This is post number one" };
  let sql = "INSERT INTO categoria ( nombre ) VALUES('" + nombre + "')";
  let query = db.query(sql, post, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log(result);
    res.send({ estado: "categoria agregada..." });
  });
});

app.post("/facultad", (req, res) => {
  let nombre = req.body.nombre;
  let post = { title: "Post facultad", body: "This is post number one" };
  let sql = "INSERT INTO facultad ( nombre) VALUES('" + nombre + "')";
  let query = db.query(sql, post, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log(result);
    res.send({ estado: "facultad agregada..." });
  });
});

app.post("/votar", (req, res) => {
  console.log(req.body);
  req.body.forEach(categoria => {
    console.log(categoria);

    let post = { title: "Post Voto", body: "This is post number one" };
    let sql =
      "INSERT INTO voto (categoria_id, mesa_id, partido_id) VALUES('" +
      categoria.categoriaId +
      "', '" +
      categoria.mesaId +
      "', '" +
      categoria.partidoId +
      "')";
    let query = db.query(sql, post, (err, result) => {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      console.log(result);
    });
  });
  res.send({ estado: "voto exitoso" });
});

app.post("/votarMongo", (req, res) => {
  console.log(req.body);
  req.body.forEach(categoria => {
    console.log(categoria);
    let voto = {
      cat: categoria.categoriaId,
      par: categoria.partidoId,
      mesa: categoria.mesaId
    };
    mongo.connect(
      url,
      function(err, db) {
        if (err) {
          return res.status(500).json({
            err: err,
            ok: false
          });
        }
        var dbo = db.db("heroku_3rzr39lq");
        dbo.collection("votos").insertOne(voto, function(err, res) {
          if (err) {
            return res.status(500).json({
              err: err,
              ok: false
            });
          }
          console.log("1 document inserted");
          db.close();
        });
      }
    );
  });
  res.send({ estado: "voto exitoso" });
});

app.post("/generarCodigos", (req, res) => {
  let numero = req.body.numero;
  let mesaId = req.body.mesaId;
  while (numero != 0) {
    let prefijo = numero + "-";
    let codigo = generatePassword(10, false, /\d/, prefijo);
    let post = { title: "Post codigo", body: "This is post number one" };
    let sql =
      "INSERT INTO codigo ( mesa_id, codigo, estado) VALUES('" +
      mesaId +
      "','" +
      codigo +
      "','true')";
    let query = db.query(sql, post, (err, result) => {
      if (err) {
        return res.status(500).json({
          err: err,
          ok: false
        });
      }
      console.log(true);
    });
    numero--;
  }
  res.send({ estado: "codigos generados..." });
});

app.post("/verificarCodigo", (req, res) => {
  let codigo = req.body.codigo;
  let mesaId = req.body.mesaId;
  console.log(req.body);
  let estado;
  let sql =
    "SELECT estado, id FROM codigo WHERE mesa_id = '" +
    mesaId +
    "' and codigo = '" +
    codigo +
    "' ";
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    if (result[0] === undefined) {
      res.send({ estado: "codigo invalido" });
    } else {
      console.log(result[0]);
      if (result[0].estado === 0) {
        let post = { title: "Post codigo", body: "This is post number one" };
        let sql2 =
          "UPDATE codigo  SET estado = 1 where id ='" + result[0].id + "'";
        db.query(sql2, post, (err, result2) => {
          if (err) {
            return res.status(500).json({
              err: err,
              ok: false
            });
          }
          console.log("codigo changed to false");
        });
        res.send({ estado: true });
      } else {
        res.send({ estado: "este codigo ya esta usado" });
      }
    }
  });
});

app.post("/cambiarEstado", (req, res) => {
  let estado = req.body.estado;
  let mesaId = req.body.mesaId;
  let date = new Date();
  let sql;
  console.log(date);
  if (estado == "1") {
    sql =
      "UPDATE mesa  SET estado = " +
      estado +
      ", iniciado = '" +
      date +
      "' where id ='" +
      mesaId +
      "'";
  } else {
    sql =
      "UPDATE mesa  SET estado = " +
      estado +
      ", terminado = '" +
      date +
      "' where id ='" +
      mesaId +
      "'";
  }
  let query = db.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    if (result[0] === undefined) {
      res.send({ estado: "codigo invalido" });
    } else {
      res.send({ estado: true });
    }
  });
});

app.post("/publicarResultados", (req, res) => {
  let estado = req.body.estado;
  console.log(req.body);
  let post = { title: "Post codigo", body: "This is post number one" };
  let sql2 = "UPDATE administrador SET publicado = " + estado + " where 1 = 1";
  db.query(sql2, post, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    console.log("resultados a estado " + estado);
    res.send({ estado: true });
  });
});

app.post("/cambiarEntregadoMesa", (req, res) => {
  let codigo = req.body.codigo;
  console.log(req.body);
  let post = { title: "Post codigo", body: "This is post number one" };
  let sql2 = "UPDATE mesa SET entregado = 1 where codigo = '" + codigo + "'";
  db.query(sql2, post, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send({ estado: true });
  });
});

app.post("/cambiarContrasena", (req, res) => {
  let codigo = req.body.codigo;
  let contra = generatePassword(5, false);
  let post = { title: "Post codigo", body: "This is post number one" };
  let sql2 =
    "UPDATE mesa SET contrasena = '" +
    contra +
    "' where codigo = '" +
    codigo +
    "'";
  db.query(sql2, post, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send({ contra: contra });
  });
});

app.get("/obtenerEstadoResultados", (req, res) => {
  let sql2 = "SELECT publicado FROM administrador where 1 = 1";
  db.query(sql2, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send({ admin: result2 });
  });
});

app.post("/dondeVoto", (req, res) => {
  let codigo = req.body.codigo;
  let post = { title: "Post codigo", body: "This is post number one" };
  let sql2 =
    "select a.codigo codigoMesa, b.codigo codigoAlumno, b.nombre nombreAlumno, c.nombre nombreFacultad from mesa a,alumno b, facultad c where b.codigo = '" +
    codigo +
    "' and b.mesa_id = a.id and a.facultad_id = c.id";
  db.query(sql2, post, (err, result2) => {
    if (err) {
      return res.status(500).json({
        err: err,
        ok: false
      });
    }
    res.send({ resultado: result2[0] });
  });
});
