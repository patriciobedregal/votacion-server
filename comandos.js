var exec = require('child_process').exec, child;

child = exec('cat file | node mapper.js | sort -k1,1 | node reducer.js',
// Pasamos los parámetros error, stdout la salida
// que mostrara el comando
  function (error, stdout, stderr) {
    // Imprimimos en pantalla con console.log
    console.log(stdout);
    // controlamos el error
    if (error !== null) {
      console.log('exec error: ' + error);
    }
});
