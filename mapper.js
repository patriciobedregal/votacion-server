#!/usr/bin/env node
'use strict;';

var streamingUtils = require('hadoop-streaming-utils');
var iterateJsonLines = streamingUtils.iterateJsonLines;
var emitJson = streamingUtils.emitJson;

iterateJsonLines(function(voto) {
    var cat = voto.cat +":"+voto.par;

        emitJson(cat,1);
});
