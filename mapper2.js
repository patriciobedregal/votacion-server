#!/usr/bin/env node
'use strict;';

var streamingUtils = require('hadoop-streaming-utils');
var iterateJsonLines = streamingUtils.iterateJsonLines;
var emitJson = streamingUtils.emitJson;

iterateJsonLines(function(tweet) {
    var cat = tweet.cat +":"+tweet.par +":"+tweet.mesa;

        emitJson(cat,1);
});
